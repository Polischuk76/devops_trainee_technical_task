# Configure the Azure provider

terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.0.2"
    }
  }

  required_version = ">= 1.2.0"
}

provider "azurerm" {
  features {}
}

# Configure resource group

resource "azurerm_resource_group" "MainTask" {
  name     = "Terraform_MainTask_ResourceGroup"
  location = "uksouth"
}

resource "azurerm_network_security_group" "MainTask_net_sec_gr" {
  name                = "MainTask_net_sec_gr"
  location            = azurerm_resource_group.MainTask.location
  resource_group_name = azurerm_resource_group.MainTask.name

  security_rule = [
    # {
    #   access                                     = "Allow"
    #   description                                = "rdp_rule"
    #   destination_address_prefix                 = "*"
    #   destination_address_prefixes               = []
    #   destination_application_security_group_ids = []
    #   destination_port_range                     = "3389"
    #   destination_port_ranges                    = []
    #   direction                                  = "Inbound"
    #   name                                       = "rdp"
    #   priority                                   = 100
    #   protocol                                   = "Tcp"
    #   source_address_prefix                      = "*"
    #   source_address_prefixes                    = []
    #   source_application_security_group_ids      = []
    #   source_port_range                          = "*"
    #   source_port_ranges                         = []
    # },
    {
      access                                     = "Allow"
      description                                = "http_rule"
      destination_address_prefix                 = "*"
      destination_address_prefixes               = []
      destination_application_security_group_ids = []
      destination_port_range                     = "80"
      destination_port_ranges                    = []
      direction                                  = "Inbound"
      name                                       = "http"
      priority                                   = 200
      protocol                                   = "Tcp"
      source_address_prefix                      = "*"
      source_address_prefixes                    = []
      source_application_security_group_ids      = []
      source_port_range                          = "*"
      source_port_ranges                         = []
    },
  ]
}