#Configure vnet and subnet

resource "azurerm_virtual_network" "MainTask_vnet" {
  name                = "MainTask-network"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.MainTask.location
  resource_group_name = azurerm_resource_group.MainTask.name
}

resource "azurerm_subnet" "subnet-VM1" {
  name                 = "subnet-VM1"
  resource_group_name  = azurerm_resource_group.MainTask.name
  virtual_network_name = azurerm_virtual_network.MainTask_vnet.name
  address_prefixes     = ["10.0.1.0/24"]
}
resource "azurerm_subnet" "subnet-VM2" {
  name                 = "subnet-VM2"
  resource_group_name  = azurerm_resource_group.MainTask.name
  virtual_network_name = azurerm_virtual_network.MainTask_vnet.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_subnet_network_security_group_association" "subnet-VM1-association" {
  subnet_id                 = azurerm_subnet.subnet-VM1.id
  network_security_group_id = azurerm_network_security_group.MainTask_net_sec_gr.id
}

resource "azurerm_subnet_network_security_group_association" "subnet-VM2-association" {
  subnet_id                 = azurerm_subnet.subnet-VM2.id
  network_security_group_id = azurerm_network_security_group.MainTask_net_sec_gr.id
}

resource "azurerm_network_interface" "nic-VM1" {
  name                = "nic-VM1"
  location            = azurerm_resource_group.MainTask.location
  resource_group_name = azurerm_resource_group.MainTask.name


  ip_configuration {
    name                          = "internal-VM1"
    subnet_id                     = azurerm_subnet.subnet-VM1.id
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_network_interface" "nic-VM2" {
  name                = "nic-VM2"
  location            = azurerm_resource_group.MainTask.location
  resource_group_name = azurerm_resource_group.MainTask.name

  ip_configuration {
    name                          = "internal-VM2"
    subnet_id                     = azurerm_subnet.subnet-VM2.id
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_network_interface_security_group_association" "nic-VM1-nsg" {
  network_interface_id      = azurerm_network_interface.nic-VM1.id
  network_security_group_id = azurerm_network_security_group.MainTask_net_sec_gr.id
}

resource "azurerm_network_interface_security_group_association" "nic-VM2-nsg" {
  network_interface_id      = azurerm_network_interface.nic-VM2.id
  network_security_group_id = azurerm_network_security_group.MainTask_net_sec_gr.id
}