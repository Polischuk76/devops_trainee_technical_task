# Configure LoadBalancer

resource "azurerm_public_ip" "MainTask_LoadBalancer_IP" {
  name                = "PublicIPForLoadBalancer"
  location            = azurerm_resource_group.MainTask.location
  resource_group_name = azurerm_resource_group.MainTask.name
  allocation_method   = "Static"
  sku                 = "Standard"
}

resource "azurerm_lb" "MainTask_LoadBalancer" {
  name                = "MainTaskLoadBalancer"
  location            = azurerm_resource_group.MainTask.location
  resource_group_name = azurerm_resource_group.MainTask.name
  sku                 = "Standard"

  frontend_ip_configuration {
    name                 = "PublicIPAddress"
    public_ip_address_id = azurerm_public_ip.MainTask_LoadBalancer_IP.id
  }
  depends_on = [
    azurerm_public_ip.MainTask_LoadBalancer_IP
  ]
}

resource "azurerm_lb_backend_address_pool" "LB_BackEndAddressPool" {

  loadbalancer_id = azurerm_lb.MainTask_LoadBalancer.id
  name            = "LB_BackEndAddressPool"
  depends_on = [
    azurerm_lb.MainTask_LoadBalancer,
    azurerm_virtual_network.MainTask_vnet
  ]

}

resource "azurerm_lb_probe" "MainTask_probe_tcp_80_port" {
  name            = "tcp-probe__80_port"
  protocol        = "Tcp"
  port            = 80
  loadbalancer_id = azurerm_lb.MainTask_LoadBalancer.id
}

resource "azurerm_lb_rule" "MainTask_80_port_lb_rule" {
  name                           = "80_port_lb_rule"
  protocol                       = "Tcp"
  frontend_port                  = 80
  backend_port                   = 80
  frontend_ip_configuration_name = azurerm_lb.MainTask_LoadBalancer.frontend_ip_configuration[0].name
  probe_id                       = azurerm_lb_probe.MainTask_probe_tcp_80_port.id
  loadbalancer_id                = azurerm_lb.MainTask_LoadBalancer.id
  backend_address_pool_ids       = [azurerm_lb_backend_address_pool.LB_BackEndAddressPool.id]
}

resource "azurerm_network_interface_backend_address_pool_association" "web_nic_VM1_lb_associate" {
  network_interface_id    = azurerm_network_interface.nic-VM1.id
  ip_configuration_name   = azurerm_network_interface.nic-VM1.ip_configuration[0].name
  backend_address_pool_id = azurerm_lb_backend_address_pool.LB_BackEndAddressPool.id
}

resource "azurerm_network_interface_backend_address_pool_association" "web_nic_VM2_lb_associate" {
  network_interface_id    = azurerm_network_interface.nic-VM2.id
  ip_configuration_name   = azurerm_network_interface.nic-VM2.ip_configuration[0].name
  backend_address_pool_id = azurerm_lb_backend_address_pool.LB_BackEndAddressPool.id
}
