$env:computername
$site = "TestSite"
$webapppool = "TestPool"
Write-Output "Web-Server: install  WebAdministration module on $env:computername"
Install-WindowsFeature -Name Web-Server -IncludeAllSubFeature -IncludeManagementTools
Write-Output "Stop IIS on $env:computername. Clean up Website, Application Pool, and site folder"
iisreset.exe /stop
Write-Output "Clean up Website, Application Pool, and Site folder"
Remove-WebAppPool -Name *
Remove-Item ([System.Environment]::ExpandEnvironmentVariables((get-website).PhysicalPath)) -Recurse -Force -Confirm:$false
Remove-WebSite -Name *
New-Item -Path "$Env:systemdrive\inetpub\" -Name $site -ItemType "directory" -Force
New-WebAppPool -Name $webapppool
New-WebSite -Name $site -Port 80 -HostHeader "" -ApplicationPool $webapppool  -PhysicalPath "$Env:systemdrive\inetpub\$site"
Set-Content -Path "$Env:systemdrive\inetpub\$site\index.html" -Value "<!DOCTYPE html><head><title>$site</title></head><body><div><h1>This is my test site $env:computername!</h1></div></body>" -Force
Write-Output "Created  Website: $site , Application Pool: $webapppool, and site folder"
(get-website).PhysicalPath
iisreset.exe /start
Write-Output "Started IIS on $env:computername"
