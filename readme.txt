Включити winrm на port 80
------------------------------------------------------------------------
winrm set winrm/config/Listener?Address=*+Transport=HTTP '@{Port="80"}'
Set-NetFirewallRule -Name "WINRM-HTTP-Compat-In-TCP" -Enabled True
або 

Set-Item WSMan:\localhost\listener\*\Port 80 -force
Set-NetFirewallRule -Name "WINRM-HTTP-Compat-In-TCP" -Enabled True
------------------------------------------------------------------------

перевірити налаштування слухача winrm
----------------------------------------------------------------------
winrm e winrm/config/Listener

на свому пк потрібно додати віддалені сервери до довірених хостів
----------------------------------------------------------------
get-Item WSMan:\localhost\Client\TrustedHosts          #глянути перелік довірених хостів

Set-Item wsman:\localhost\client\TrustedHosts -Value <host|ip> -Concatenate -Force # додати хост чи ір (-Concatenate додати до існуючих)
